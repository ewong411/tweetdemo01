package com.kenfogel.tweetdemo01.presentation;

import com.kenfogel.tweetdemo01.business.TwitterEngine;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * @author Ken Fogel
 */
public class TweetDemo01 extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TweetDemo01.class);
    
    /**
     * Where it begins
     * @param args 
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    private TextArea textArea;
    private TextArea textArea2;
    private TextField textField;
    private BorderPane dm;
    private BorderPane tweet;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    
    
    /**
     * JavaFX begins at start
     * 
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Set window's title
        primaryStage.setTitle("Send a tweet");
        
        StackPane root = new StackPane();
        tweet = new BorderPane();
        dm = new BorderPane();
        
        dm.setBottom(makeButtons2());
        dm.setTop(recipientBox());
        tweet.setBottom(makeButtons());
        
        textArea = new TextArea();
        textArea.setWrapText(true);
        
        textArea2 = new TextArea();
        textArea2.setWrapText(true);
        tweet.setCenter(textArea);  
        dm.setCenter(textArea2);
        
        root.getChildren().add(dm);
        root.getChildren().add(tweet);

        
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }


    /**
     * Create an HBox of buttons
     * 
     * @return the constructed hbox
     */
    private HBox makeButtons() /*throws TwitterException*/{
        HBox hbox = new HBox();
        Button send = new Button("Send");
        send.setOnAction(event -> {
            try {
                if (textArea.getText().length() > 280) {
                    LOG.error("Cannot Send A Tweet with more then 280 characters");
                    send.setDisable(true);
                }else{
                    send.setDisable(false);
                    twitterEngine.createTweet(textArea.getText());
                }              

            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
            }
        });
        Font font = new Font(18);
        send.setFont(font);

        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        exit.setFont(font);
        
        Button switchB = new Button("Switch");
        switchB.setOnAction(event -> {
        if(dm.isVisible()){             
             dm.setVisible(false);
             tweet.setVisible(true);
        }else{
            dm.setVisible(true);
            tweet.setVisible(false);      
        }
        });
        switchB.setFont(font);
        
        
        hbox.getChildren().add(send);
        hbox.getChildren().add(exit);
        hbox.getChildren().add(switchB);

        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }
    private HBox makeButtons2() /*throws TwitterException*/{
        HBox hbox = new HBox();
        Button send = new Button("Send");
        send.setOnAction(event -> {
            try {
                if (textArea.getText().length() > 280) {
                    LOG.error("Cannot Send A Tweet with more then 280 characters");
                    send.setDisable(true);
                }else{
                    send.setDisable(false);
                    twitterEngine.sendDirectMessage(textField.getText(),textArea2.getText());
                }              

            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
            }
        });
        Font font = new Font(18);
        send.setFont(font);

        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        exit.setFont(font);
        
        Button switchB = new Button("Switch");
        switchB.setOnAction(event -> {
        if(dm.isVisible()){             
             dm.setVisible(false);
             tweet.setVisible(true);
        }else{
            dm.setVisible(true);
            tweet.setVisible(false);      
        }
        });
        switchB.setFont(font);
        
        hbox.getChildren().add(send);
        hbox.getChildren().add(exit);
        hbox.getChildren().add(switchB);

        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }
    private HBox recipientBox(){
        HBox hbox = new HBox();
        textField = new TextField();
        Label recipient = new Label();
        recipient.setText("Recipient:");
        hbox.getChildren().add(recipient);
        hbox.getChildren().add(textField);
        
        hbox.setAlignment(Pos.TOP_LEFT);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }

}
